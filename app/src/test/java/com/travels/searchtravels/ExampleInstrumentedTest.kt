package com.travels.searchtravels

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.travels.searchtravels.activity.MainActivity

import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun createActivity() {

        val scenario = launch(MainActivity::class.java)
        //сначала жмем на ясно-понятно
        onView(withId(R.id.nextBTN))
            .perform(click());
        //потом выбираем фотку
        onView(withId(R.id.pickImageBTN))
            .perform(click())
            .check(matches(isDisplayed()));
        //выбираем фото

    }
}